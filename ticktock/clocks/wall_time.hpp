#pragma once

#include <ticktock/clocks/time_units.hpp>

#include <ostream>

namespace ticktock {

///////////////////////////////////////////////////////////////////////////

class WallTime {
 public:
  WallTime(TimeUnits since_epoch) : value_(since_epoch) {
  }

  WallTime& operator+=(TimeUnits delta) {
    value_ += delta;
    return *this;
  }

  TimeUnits TimeSinceEpoch() const {
    return value_;
  }

 private:
  TimeUnits value_;
};

///////////////////////////////////////////////////////////////////////////

// Arithmetics

inline WallTime operator+(WallTime time_point, TimeUnits delta) {
  return WallTime{time_point.TimeSinceEpoch() + delta};
}

// Precondition: lhs >= rhs
inline TimeUnits operator-(WallTime lhs, WallTime rhs) {
  return lhs.TimeSinceEpoch() - rhs.TimeSinceEpoch();
}

///////////////////////////////////////////////////////////////////////////

// Comparison

inline bool operator==(WallTime lhs, WallTime rhs) {
  return lhs.TimeSinceEpoch() == rhs.TimeSinceEpoch();
}

inline bool operator!=(WallTime lhs, WallTime rhs) {
  return !(lhs == rhs);
}

inline bool operator<(WallTime lhs, WallTime rhs) {
  return lhs.TimeSinceEpoch() < rhs.TimeSinceEpoch();
}

inline bool operator<=(WallTime lhs, WallTime rhs) {
  return lhs.TimeSinceEpoch() <= rhs.TimeSinceEpoch();
}

inline bool operator>(WallTime lhs, WallTime rhs) {
  return lhs.TimeSinceEpoch() > rhs.TimeSinceEpoch();
}

inline bool operator>=(WallTime lhs, WallTime rhs) {
  return lhs.TimeSinceEpoch() >= rhs.TimeSinceEpoch();
}

///////////////////////////////////////////////////////////////////////////

// Pretty printing

inline std::ostream& operator<<(std::ostream& out, const WallTime& time_point) {
  out << 'T' << time_point.TimeSinceEpoch().count();
  return out;
}

}  // namespace ticktock
