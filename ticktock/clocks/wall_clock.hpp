#pragma once

#include <ticktock/clocks/wall_time.hpp>

namespace ticktock {

struct IWallClock {
  virtual ~IWallClock() = default;

  virtual WallTime Now() const = 0;

  // Utilities

  WallTime ToDeadline(TimeUnits timeout) {
    return Now() + timeout;
  }

  std::chrono::milliseconds ToTimeout(WallTime deadline) {
    auto now = Now();
    if (deadline < now) {
      return std::chrono::milliseconds{0};
    } else {
      return std::chrono::duration_cast<std::chrono::milliseconds>(deadline - now);
    }
  }
};

}  // namespace ticktock
