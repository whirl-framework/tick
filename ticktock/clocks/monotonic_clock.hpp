#pragma once

#include <ticktock/clocks/monotonic_time.hpp>

namespace ticktock {

struct IMonotonicClock {
  virtual ~IMonotonicClock() = default;

  virtual MonotonicTime Now() const = 0;
};

}  // namespace ticktock
