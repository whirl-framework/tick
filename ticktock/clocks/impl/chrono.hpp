#pragma once

#include <ticktock/clocks/monotonic_clock.hpp>
#include <ticktock/clocks/wall_clock.hpp>

namespace ticktock::impl::std {

//////////////////////////////////////////////////////////////////////

// Wall clock

class SystemClock : public IWallClock {
 public:
  WallTime Now() const override;
};

//////////////////////////////////////////////////////////////////////

// Monotonic clock

class SteadyClock : public IMonotonicClock {
 public:
  MonotonicTime Now() const override;
};

}  // namespace ticktock::impl::std
