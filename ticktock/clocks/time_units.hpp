#pragma once

#include <chrono>

namespace ticktock {

using TimeUnits = std::chrono::nanoseconds;

}  // namespace ticktock
