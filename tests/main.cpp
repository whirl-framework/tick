#include <ticktock/clocks/impl/chrono.hpp>

#include <thread>
#include <iostream>

using namespace std::chrono_literals;

//////////////////////////////////////////////////////////////////////

ticktock::IMonotonicClock& SteadyClock() {
  static ticktock::impl::std::SteadyClock impl;
  return impl;
}

ticktock::IWallClock& SystemClock() {
  static ticktock::impl::std::SystemClock impl;
  return impl;
}

//////////////////////////////////////////////////////////////////////

void MonotonicNowTest() {
  auto t1 = SteadyClock().Now();

  std::this_thread::sleep_for(1s);

  auto t2 = SteadyClock().Now();

  assert(t2 - t1 >= 500ms);
}

void WallNowTest() {
  auto t1 = SystemClock().Now();

  std::this_thread::sleep_for(1s);

  auto t2 = SystemClock().Now();

  assert(t1 <= t2);
  assert(t2 >= t1);

  assert(t1 < t2);
  assert(t2 > t1);

  assert(t1 != t2);
  assert(t1 == t1);

  assert(t2 - t1 >= 500ms);

  assert(t1 + 500ms <= t2);
}

void TimeoutsTest() {
  auto now = SystemClock().Now();

  auto deadline = SystemClock().ToDeadline(1s);

  assert(deadline - now >= 1s);

  auto timeout = SystemClock().ToTimeout(deadline);

  assert(timeout <= 1s);
  assert(timeout >= 500ms);
}

//////////////////////////////////////////////////////////////////////

int main() {
  MonotonicNowTest();
  WallNowTest();
  TimeoutsTest();
  return 0;
}
