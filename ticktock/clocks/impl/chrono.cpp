#include <ticktock/clocks/impl/chrono.hpp>

#include <chrono>

namespace ticktock::impl::std {

//////////////////////////////////////////////////////////////////////

// Wall clock

WallTime SystemClock::Now() const {
  auto wall_now = ::std::chrono::system_clock::now();
  return {wall_now.time_since_epoch()};
}

//////////////////////////////////////////////////////////////////////

// Monotonic clock

MonotonicTime SteadyClock::Now() const {
  auto steady_now = ::std::chrono::steady_clock::now();
  return {steady_now.time_since_epoch()};
}

}  // namespace ticktock::impl::std
