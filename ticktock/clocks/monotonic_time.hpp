#pragma once

#include <ticktock/clocks/time_units.hpp>

namespace ticktock {

///////////////////////////////////////////////////////////////////////////

class MonotonicTime {
 public:
  MonotonicTime(TimeUnits since_reset)
      : value_(since_reset) {
  }

  // Time elapsed since monotonic clock reset
  TimeUnits TimeSinceReset() const {
    return value_;
  }

 private:
  TimeUnits value_;
};

///////////////////////////////////////////////////////////////////////////

inline TimeUnits operator-(MonotonicTime lhs, MonotonicTime rhs) {
  return lhs.TimeSinceReset() - rhs.TimeSinceReset();
}

}  // namespace ticktock
