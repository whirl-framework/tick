#pragma once

#include <ticktock/clocks/wall_time.hpp>

namespace ticktock {

// TrueTime from Google Spanner
// Spanner: Google's Globally-Distributed Database
// https://research.google/pubs/pub39966/

// [earliest, latest]
struct TTInterval {
  WallTime earliest;
  WallTime latest;
};

struct ITrueTimeClock {
  virtual ~ITrueTimeClock() = default;

  // Returns a TTInterval that is guaranteed to contain
  // the absolute time during which TT.Now() was invoked
  virtual TTInterval Now() const = 0;

  // True if `time_point` has definitely passed
  bool After(WallTime time_point) const {
    return Now().earliest > time_point;
  }

  // True if `time_point` has definitely not arrived
  bool Before(WallTime time_point) const {
    return Now().latest < time_point;
  }
};

}  // namespace ticktock
